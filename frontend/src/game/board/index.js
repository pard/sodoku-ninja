import React, { useContext } from "react";
import "./index.css";
import SodokuContext from "../../context";

const Cell = ({ value }) => {
  return value === " " ? (
    <div className="cell blank">_</div>
  ) : (
    <div className="cell">{value}</div>
  );
};

export const Board = () => {
  const board = useContext(SodokuContext);
  return (
    <div className="board">
      {board.map((square_rows) => {
        return square_rows.map((square) => {
          return (
            <div className="square">
              {square.map((row) => {
                return row.map((col) => <Cell value={col} />);
              })}
            </div>
          );
        });
      })}
    </div>
  );
};
