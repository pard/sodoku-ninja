import React, { useContext } from "react";
import { Board } from "./board";
import SodokuContext from "../context";

export const Game = () => {
  const board = useContext(SodokuContext);
  return <Board board={board} />;
};
