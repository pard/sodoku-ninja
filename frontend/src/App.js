import React from "react";
import "./App.css";
import { Game } from "./game";
import { load } from "./loader";
import { SodokuProvider } from "./context";

function App() {
  let board = load();
  return (
    <SodokuProvider value={board}>
      <div className="game">
        <Game />
      </div>
    </SodokuProvider>
  );
}

export default App;
