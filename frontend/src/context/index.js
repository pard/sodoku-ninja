import React from "react";

const SodokuContext = React.createContext({});

export const SodokuProvider = SodokuContext.Provider;

export default SodokuContext;
