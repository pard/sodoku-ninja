export const load = () => {
  let puzzle = "016400000200009000400000062070230100100000003003087040960000005000800007000006820".replaceAll(
    "0",
    " "
  );
  //"123456789578139624496872153952381467641297835387564291719623548864915372235748916";
  const squares = [];
  for (let i = 9; i < puzzle.length + 1; i++) {
    if (i % 9 === 0) {
      const row = puzzle.slice(i - 9, i);
      for (var j = 3; j < row.length + 1; j++) {
        if (j % 3 === 0) {
          const numbers = row.slice(j - 3, j).split("");
          squares.push(numbers);
        }
      }
    }
  }
  const board = [
    [[], [], []],
    [[], [], []],
    [[], [], []],
  ];
  let board_index = 0;
  let square_col_index = 0;
  for (let i = 0; i < squares.length; i++) {
    let numbers = squares[i];
    board_index = i % 9 === 0 && i !== 0 ? board_index + 1 : board_index;
    board[board_index][square_col_index].push(numbers);
    square_col_index =
      square_col_index % 2 === 0 && square_col_index !== 0
        ? 0
        : square_col_index + 1;
  }
  return board;
};
